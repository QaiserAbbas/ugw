//
//  BackendBase.swift
//  SafeCabApp
//
//  Created by SafeCab on 1/3/17.
//  Copyright © 2017 SafeCab. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import CoreLocation
class BackendBase: NSObject
{
    func hardProcessingWithString(input: String, completion: (_ result: String) -> Void) {
    
            completion("we finished!")
    }
    class func accessApiByGet(apiPath:String, parameters:[String : AnyObject]?, Success: @escaping ((_ result : Any) -> Void) , Failure : @escaping ((_ error : Error) -> Void)){
        
        print(apiPath)
        let safeURL = apiPath.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        Alamofire.request(safeURL!)
            .validate()
            .responseJSON { response in
               // print(response)
               // CompletionHandler(response.result.value as! [String : AnyObject])
                switch response.result {
                case .success:
                    print("Validation Successful")
        
                    if let json = response.result.value {
                        print(json)
                     Success(json)
                    }
                case .failure(let error):
                    Failure(error)
                    print(error)
                }
        }

    
    }
    
    class func accessApiByPost(apiPath:String, parameters:[String : AnyObject]?, Success: @escaping ((_ result : Any) -> Void) , Failure : @escaping ((_ error : Error) -> Void)){
        
        let safeURL = apiPath.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        Alamofire.request(safeURL! , method: .post , parameters:parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    print("Validation Successful")
                    
                    if let json = response.result.value {
                        //print(json)
                        Success(json)
                    }
                case .failure(let error):
                    Failure(error)
                   // print(error)
                }
        }
        
        
    }
    class func uploadApiByPost(apiPath:String, parameters:[String : AnyObject]?, fileData:[String : Any], Success: @escaping ((_ result : Any) -> Void) , Failure : @escaping ((_ error : Error) -> Void)){
        
        let safeURL = apiPath.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        let URL = try! URLRequest(url: safeURL!, method: .post)

           Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(UIImageJPEGRepresentation(fileData["file"] as! UIImage, 0.5)!, withName: "PersonPhoto", fileName: "profile", mimeType: "image")
            
        }, with: URL, encodingCompletion: { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                   // self.delegate?.showSuccessAlert()
                    print(response.request!)  // original URL request
                    print(response.response!) // URL response
                    print(response.data!)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                   // self.removeImage("frame", fileExtension: "txt")
                    if let JSON = response.result.value {
                        Success(JSON)
                        //print("JSON: \(JSON)")
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                Failure(encodingError)
            }
            
        })
        
        
    }

   class func ShowMBProgress(view:UIView)
   {
        let loadingNotification = MBProgressHUD.showAdded(to: view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"

    }
   class func HideMBProgress(view: UIView)
   {
        MBProgressHUD.hide(for: view, animated: true)
    }
    
}

//
//  ModelManager.swift
//  DataBaseDemo
//
//  Created by Krupa-iMac on 05/08/14.
//  Copyright (c) 2014 TheAppGuruz. All rights reserved.
//

import UIKit

let sharedInstance = ModelManager()

class ModelManager: NSObject {
    
    var publicDB: FMDatabase? = nil
    var promo: FMDatabase? = nil
    var sales: FMDatabase? = nil
    var nestle: FMDatabase? = nil
    var bsh: FMDatabase? = nil
    var weber: FMDatabase? = nil
    var tcfhe: FMDatabase? = nil
    var colibri: FMDatabase? = nil
    var communication: FMDatabase? = nil

    class func getInstance() -> ModelManager
    {
        if(sharedInstance.publicDB == nil)
        {
            sharedInstance.publicDB = FMDatabase(path: Util.getPath("public.sqlite"))
        }
        if(sharedInstance.promo == nil)
        {
            sharedInstance.promo = FMDatabase(path: Util.getPath("promo.sqlite"))
        }
        if(sharedInstance.sales == nil)
        {
            sharedInstance.sales = FMDatabase(path: Util.getPath("sales.sqlite"))
        }
        if(sharedInstance.nestle == nil)
        {
            sharedInstance.nestle = FMDatabase(path: Util.getPath("nestle.sqlite"))
        }
        if(sharedInstance.bsh == nil)
        {
            sharedInstance.bsh = FMDatabase(path: Util.getPath("bsh.sqlite"))
        }
        if(sharedInstance.weber == nil)
        {
            sharedInstance.weber = FMDatabase(path: Util.getPath("weber.sqlite"))
        }
        if(sharedInstance.tcfhe == nil)
        {
            sharedInstance.tcfhe = FMDatabase(path: Util.getPath("tcfhe.sqlite"))
        }
        if(sharedInstance.colibri == nil)
        {
            sharedInstance.colibri = FMDatabase(path: Util.getPath("colibri.sqlite"))
        }
        if(sharedInstance.communication == nil)
        {
            sharedInstance.communication = FMDatabase(path: Util.getPath("communication.sqlite"))
        }
        
        return sharedInstance
    }
    
    //        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE student_info SET Name=?, Marks=? WHERE RollNo=?", withArgumentsIn: [studentInfo.Name, studentInfo.Marks, studentInfo.RollNo])
    //        sharedInstance.database!.close()

        func updateAvailabilityForAvailable(_ status: String , date: String, dateToUpdate: Date) -> Bool {
            sharedInstance.publicDB!.open()
            let isUpdated = sharedInstance.publicDB!.executeUpdate("UPDATE r_kollegen_zeit SET arbeitstatus=? WHERE arbeitsdatum=?", withArgumentsIn: [status,date])
            sharedInstance.publicDB!.close()
            return isUpdated
        }
    func updateAvailabilityForAccepted(_ status: String , date: String, dateToUpdate: Date) -> Bool {
        sharedInstance.publicDB!.open()
        let isUpdated = sharedInstance.publicDB!.executeUpdate("UPDATE r_kollegen_zeit SET arbeitstatus=?, z_confirmed=? WHERE arbeitsdatum=?", withArgumentsIn: [status,date,date])
        sharedInstance.publicDB!.close()
        return isUpdated
    }


    func save_publicDB(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.publicDB!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.publicDB!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.publicDB!.close()
        return isInserted
    }
    
    func save_promo(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.promo!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.promo!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.promo!.close()
        return isInserted
    }
    
    func save_sales(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.sales!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.sales!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.sales!.close()
        return isInserted
    }
    
    func save_nestle(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.nestle!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.nestle!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.nestle!.close()
        return isInserted
    }
    
    func save_bsh(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.bsh!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.bsh!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.bsh!.close()
        return isInserted
    }
    
    func save_weber(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.weber!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.weber!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.weber!.close()
        return isInserted
    }
    
    func save_tcfhe(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.tcfhe!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.tcfhe!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.tcfhe!.close()
        return isInserted
    }
    
    func save_colibri(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.colibri!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.colibri!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.colibri!.close()
        return isInserted
    }
    
    func save_communication(_ dataArray: NSArray , tabelName: String) -> Bool
    {
        sharedInstance.communication!.open()
        var isInserted = false
        for data in dataArray
        {
            let dataDict = data as! NSDictionary
            var str: String = "("
            var strNew: String = "("
            for i in 0..<dataDict.allKeys.count
            {
                str += "\(dataDict.allKeys[i]),"
                strNew += "?,"
            }
            var truncated = str.substring(to: str.index(before: str.endIndex))
            var truncatedNew = strNew.substring(to: strNew.index(before: strNew.endIndex))
            
            truncated += ")"
            truncatedNew += ")"
            
            isInserted = sharedInstance.communication!.executeUpdate("INSERT INTO \(tabelName) \(truncated) VALUES \(truncatedNew)", withArgumentsIn: dataDict.allValues)
        }
        sharedInstance.communication!.close()
        return isInserted
    }
    
    
    
    func getAllCalendarData() -> NSMutableArray
    {
        sharedInstance.publicDB!.open()
        let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT * FROM r_kollegen_zeit", withArgumentsIn: nil)
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                
//                studentInfo.setValue(resultSet.int(forColumn: "kollegenr"), forKey: "kollegenr")
//                studentInfo.setValue(resultSet.date(forColumn: "arbeitsdatum"), forKey: "arbeitsdatum")
//                studentInfo.setValue(resultSet.string(forColumn: "arbeitstatus"), forKey: "arbeitstatus")
//                studentInfo.setValue(resultSet.string(forColumn: "statuskomment"), forKey: "statuskomment")
//                studentInfo.setValue(resultSet.date(forColumn: "vonuhr"), forKey: "vonuhr")
//                studentInfo.setValue(resultSet.date(forColumn: "bisuhr"), forKey: "bisuhr")
//                studentInfo.setValue(resultSet.string(forColumn: "euserid"), forKey: "euserid")
//                studentInfo.setValue(resultSet.date(forColumn: "edatum"), forKey: "edatum")
//                studentInfo.setValue(resultSet.date(forColumn: "adatum"), forKey: "adatum")
//                studentInfo.setValue(resultSet.date(forColumn: "z_confirmed"), forKey: "z_confirmed")
//                studentInfo.setValue(resultSet.string(forColumn: "auserid"), forKey: "auserid")
//                studentInfo.setValue(resultSet.int(forColumn: "serial"), forKey: "serial")
                marrStudentInfo.add(studentInfo)
            }
        }
        sharedInstance.publicDB!.close()
        return marrStudentInfo
    }
    
    func getAllVisitData() -> NSMutableArray
    {
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        marrStudentInfo.addObjects(from: fetchFrombsh() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFromcolibri() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFromcommunication() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFromnestle() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFrompromo() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFromsales() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFromtcfhe() as [AnyObject])
        marrStudentInfo.addObjects(from: fetchFromweber() as [AnyObject])

        return marrStudentInfo
    }
    
    func fetchFrombsh() -> NSMutableArray
    {
        sharedInstance.bsh!.open()
        
        var resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("bsh", forKey: "table")

                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.bsh!.close()
        resultSet = nil
        return marrStudentInfo
    }
    
    func fetchFromcolibri() -> NSMutableArray
    {
        sharedInstance.colibri!.open()
        
        var resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("colibri", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.colibri!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchFromcommunication() -> NSMutableArray
    {
        sharedInstance.communication!.open()
        
        var resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("communication", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.communication!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchFromnestle() -> NSMutableArray
    {
        sharedInstance.nestle!.open()
        
        var resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("nestle", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.nestle!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchFrompromo() -> NSMutableArray
    {
        sharedInstance.promo!.open()
        
        var resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("promo", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.promo!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchFromsales() -> NSMutableArray
    {
        sharedInstance.sales!.open()
        
        var resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("sales", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.sales!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchFromtcfhe() -> NSMutableArray
    {
        sharedInstance.tcfhe!.open()
        
        var resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("tcfhe", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.tcfhe!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchFromweber() -> NSMutableArray
    {
        sharedInstance.weber!.open()
        
        var resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT DISTINCT * FROM `p_besuchsbericht` LEFT OUTER JOIN `r_status` ON (`p_besuchsbericht`.`statusnr` = `r_status`.`statusnr`) LEFT OUTER JOIN `s_besuchad` ON (`p_besuchsbericht`.`besuchsnr` = `s_besuchad`.`besuchsnr`) LEFT OUTER JOIN `r_outlet` ON (`p_besuchsbericht`.`outletnr` = `r_outlet`.`outletnr`)", withArgumentsIn: nil)
        
        
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        if (resultSet != nil)
        {
            while resultSet.next()
            {
                var studentInfo : NSDictionary = NSDictionary()
                studentInfo = resultSet.resultDict() as NSDictionary
                let mutableNsDict = studentInfo.mutableCopy() as! NSMutableDictionary
                mutableNsDict.setValue("weber", forKey: "table")
                
                marrStudentInfo.add(mutableNsDict)
            }
        }
        sharedInstance.weber!.close()
        resultSet = nil

        return marrStudentInfo
    }
    func fetchVisitinformation(tabelName: String, besuchsnr : Int?) -> NSMutableArray
    {
        //        besuchsnr
        if tabelName == "publicDB"
        {
            sharedInstance.publicDB!.open()
            
            let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.publicDB!.close()
            return marrStudentInfo
  
        }
        else if tabelName == "promo"
        {
            sharedInstance.promo!.open()
            
            let resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.promo!.close()
            return marrStudentInfo

        }
        else if tabelName == "sales"
        {
            sharedInstance.sales!.open()
            
            let resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.sales!.close()
            return marrStudentInfo

        }
        else if tabelName == "nestle"
        {
            sharedInstance.nestle!.open()
            
            let resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.nestle!.close()
            return marrStudentInfo
 
        }
        else if tabelName == "bsh"
        {
            sharedInstance.bsh!.open()
            
            let resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.bsh!.close()
            return marrStudentInfo
 
        }
        else if tabelName == "weber"
        {
            sharedInstance.weber!.open()
            
            let resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.weber!.close()
            return marrStudentInfo

        }
        else if tabelName == "tcfhe"
        {
            sharedInstance.tcfhe!.open()
            
            let resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.tcfhe!.close()
            return marrStudentInfo

        }
        else if tabelName == "colibri"
        {
            sharedInstance.colibri!.open()
            
            let resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.colibri!.close()
            return marrStudentInfo

        }
        else if tabelName == "communication"
        {
            sharedInstance.communication!.open()
            
            let resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT DISTINCT * FROM p_besuchsbericht WHERE besuchsnr=\(besuchsnr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.communication!.close()
            return marrStudentInfo

        }
        let marrStudentInfo : NSMutableArray = NSMutableArray()

        return marrStudentInfo
    }
    
    func fetchStudyinformation(tabelName: String, studienr : Int?) -> NSMutableArray
    {
        //        besuchsnr
        if tabelName == "publicDB"
        {
            sharedInstance.publicDB!.open()
            
            let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.publicDB!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "promo"
        {
            sharedInstance.promo!.open()
            
            let resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.promo!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "sales"
        {
            sharedInstance.sales!.open()
            
            let resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.sales!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "nestle"
        {
            sharedInstance.nestle!.open()
            
            let resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.nestle!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "bsh"
        {
            sharedInstance.bsh!.open()
            
            let resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.bsh!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "weber"
        {
            sharedInstance.weber!.open()
            
            let resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.weber!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "tcfhe"
        {
            sharedInstance.tcfhe!.open()
            
            let resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.tcfhe!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "colibri"
        {
            sharedInstance.colibri!.open()
            
            let resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.colibri!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "communication"
        {
            sharedInstance.communication!.open()
            
            let resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT DISTINCT * FROM p_study WHERE studienr=\(studienr!)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.communication!.close()
            return marrStudentInfo
            
        }
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        
        return marrStudentInfo
    }

    func fetchBlockTypeandNumbr(tabelName: String, studienr : Int?) -> NSMutableArray
    {
        if tabelName == "publicDB"
        {
            sharedInstance.publicDB!.open()
            
            let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.publicDB!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "promo"
        {
            sharedInstance.promo!.open()
            
            let resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!)  ORDER BY block_number, block_type)", withArgumentsIn:nil)
            print(resultSet)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.promo!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "sales"
        {
            sharedInstance.sales!.open()
            
            let resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.sales!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "nestle"
        {
            sharedInstance.nestle!.open()
            
            let resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.nestle!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "bsh"
        {
            sharedInstance.bsh!.open()
            
            let resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.bsh!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "weber"
        {
            sharedInstance.weber!.open()
            
            let resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.weber!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "tcfhe"
        {
            sharedInstance.tcfhe!.open()
            
            let resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.tcfhe!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "colibri"
        {
            sharedInstance.colibri!.open()
            
            let resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.colibri!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "communication"
        {
            sharedInstance.communication!.open()
            
            let resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT * FROM (SELECT DISTINCT 'p' AS block_type, pagenr AS block_number FROM s_produktstudie WHERE ebene = 0 AND studienr = \(studienr!) UNION SELECT DISTINCT 'f' AS block_type, pagenr AS block_number FROM s_fragestudie WHERE ebene = 0 AND studienr = \(studienr!))", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.communication!.close()
            return marrStudentInfo
            
        }
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        
        return marrStudentInfo
  
    }
    
    func fetchDataOfBlockType(tabelName: String, studienr : Int?, innerName : String, pagenr : Int?,comesFrom : String) -> NSMutableArray
    {
        var queryString="\(innerName) WHERE ebene = 0 AND studienr = \(studienr!) AND pagenr = \(pagenr!)"
        if comesFrom == "f"
        {
            queryString="\(innerName) WHERE active AND ebene = 0 AND studienr = \(studienr!) AND pagenr = \(pagenr!)"
        }
        else
        {
            queryString="\(innerName) WHERE ebene = 0 AND studienr = \(studienr!) AND pagenr = \(pagenr!)"
        }
        if tabelName == "publicDB"
        {
            sharedInstance.publicDB!.open()
            
            let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.publicDB!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "promo"
        {
            sharedInstance.promo!.open()
            
            let resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            print(resultSet)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.promo!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "sales"
        {
            sharedInstance.sales!.open()
            
            let resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.sales!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "nestle"
        {
            sharedInstance.nestle!.open()
            
            let resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.nestle!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "bsh"
        {
            sharedInstance.bsh!.open()
            
            let resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.bsh!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "weber"
        {
            sharedInstance.weber!.open()
            
            let resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.weber!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "tcfhe"
        {
            sharedInstance.tcfhe!.open()
            
            let resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.tcfhe!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "colibri"
        {
            sharedInstance.colibri!.open()
            
            let resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.colibri!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "communication"
        {
            sharedInstance.communication!.open()
            
            let resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.communication!.close()
            return marrStudentInfo
            
        }
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        
        return marrStudentInfo
        
    }
    func fetchMCQs(tabelName: String, studienr : Int?, innerName : String, fragenr : Int?) -> NSMutableArray
    {
        let queryString="\(innerName) WHERE studienr = \(studienr!) AND fragenr = \(fragenr!)"

        if tabelName == "publicDB"
        {
            sharedInstance.publicDB!.open()
            
            let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.publicDB!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "promo"
        {
            sharedInstance.promo!.open()
            
            let resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            print(resultSet)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.promo!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "sales"
        {
            sharedInstance.sales!.open()
            
            let resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.sales!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "nestle"
        {
            sharedInstance.nestle!.open()
            
            let resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.nestle!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "bsh"
        {
            sharedInstance.bsh!.open()
            
            let resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.bsh!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "weber"
        {
            sharedInstance.weber!.open()
            
            let resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.weber!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "tcfhe"
        {
            sharedInstance.tcfhe!.open()
            
            let resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.tcfhe!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "colibri"
        {
            sharedInstance.colibri!.open()
            
            let resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.colibri!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "communication"
        {
            sharedInstance.communication!.open()
            
            let resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.communication!.close()
            return marrStudentInfo
            
        }
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        
        return marrStudentInfo
        
    }
    func fetchColumns(tabelName: String, studienr : Int?, innerName : String, pagenr : Int?) -> NSMutableArray
    {
        let queryString="\(innerName) WHERE studienr = \(studienr!) AND p_pagenr = \(pagenr!) AND p_ebene = 0"

        if tabelName == "publicDB"
        {
            sharedInstance.publicDB!.open()
            
            let resultSet: FMResultSet! = sharedInstance.publicDB!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.publicDB!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "promo"
        {
            sharedInstance.promo!.open()
            
            let resultSet: FMResultSet! = sharedInstance.promo!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            print(resultSet)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.promo!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "sales"
        {
            sharedInstance.sales!.open()
            
            let resultSet: FMResultSet! = sharedInstance.sales!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.sales!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "nestle"
        {
            sharedInstance.nestle!.open()
            
            let resultSet: FMResultSet! = sharedInstance.nestle!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.nestle!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "bsh"
        {
            sharedInstance.bsh!.open()
            
            let resultSet: FMResultSet! = sharedInstance.bsh!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.bsh!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "weber"
        {
            sharedInstance.weber!.open()
            
            let resultSet: FMResultSet! = sharedInstance.weber!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.weber!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "tcfhe"
        {
            sharedInstance.tcfhe!.open()
            
            let resultSet: FMResultSet! = sharedInstance.tcfhe!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.tcfhe!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "colibri"
        {
            sharedInstance.colibri!.open()
            
            let resultSet: FMResultSet! = sharedInstance.colibri!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.colibri!.close()
            return marrStudentInfo
            
        }
        else if tabelName == "communication"
        {
            sharedInstance.communication!.open()
            
            let resultSet: FMResultSet! = sharedInstance.communication!.executeQuery("SELECT DISTINCT * FROM \(queryString)", withArgumentsIn:nil)
            
            
            let marrStudentInfo : NSMutableArray = NSMutableArray()
            if (resultSet != nil)
            {
                while resultSet.next()
                {
                    var studentInfo : NSDictionary = NSDictionary()
                    studentInfo = resultSet.resultDict() as NSDictionary
                    marrStudentInfo.add(studentInfo)
                }
            }
            sharedInstance.communication!.close()
            return marrStudentInfo
            
        }
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        
        return marrStudentInfo
        
    }
}


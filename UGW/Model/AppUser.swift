//
//  AppUser.swift
//  UGW
//
//  Created by Qaiser Abbas on 04/02/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import Foundation
import UIKit
let SharedAppUser : AppUser = AppUser();

class AppUser :  NSObject, NSCoding
{
    var Active : String? = "";
    var email : String? = "";
    var GDINR : String? = "";
    var UserID : String? = "";
    var FirstName : String? = "";
    var LastName : String? = "";

    var AD : Bool? = nil;
    var Kunde : Bool? = nil;
    var isUserAdmin : Bool? = nil;
    var Datenschutz : Bool? = nil;
    
    var AccessLevel : NSNumber? = nil;
    var Kollegenr : NSNumber? = nil;
    var UnitID : NSNumber? = nil;
    var GDINRSPOT : NSNumber? = nil;
    var UserNR : NSNumber? = nil;
    var id_mitarbeiter : NSNumber? = nil;

    override init()
    {
        super.init();
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init()

        self.Active = aDecoder.decodeObject(forKey: "Active") as? String;
        self.email = aDecoder.decodeObject(forKey: "email") as? String;
//        self.GDINR = aDecoder.decodeObject(forKey: "GDINR") as? String;
        self.UserID = aDecoder.decodeObject(forKey: "UserID") as? String;
        self.FirstName = aDecoder.decodeObject(forKey: "FirstName") as? String;
        self.LastName = aDecoder.decodeObject(forKey: "LastName") as? String;
        
        self.AD = aDecoder.decodeObject(forKey: "AD") as? Bool;
        self.Kunde = aDecoder.decodeObject(forKey: "Kunde") as? Bool;
        self.isUserAdmin = aDecoder.decodeObject(forKey: "isUserAdmin") as? Bool;
        self.Datenschutz = aDecoder.decodeObject(forKey: "Datenschutz") as? Bool;
        
        self.AccessLevel = aDecoder.decodeObject(forKey: "AccessLevel") as? NSNumber;
        self.Kollegenr = aDecoder.decodeObject(forKey: "Kollegenr") as? NSNumber;
        self.UnitID = aDecoder.decodeObject(forKey: "UnitID") as? NSNumber;
        self.GDINRSPOT = aDecoder.decodeObject(forKey: "GDINRSPOT") as? NSNumber;
        self.UserNR = aDecoder.decodeObject(forKey: "UserNR") as? NSNumber;
        self.id_mitarbeiter = aDecoder.decodeObject(forKey: "id_mitarbeiter") as? NSNumber;
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.Active, forKey: "Active");
        aCoder.encode(self.email, forKey: "email");
//        aCoder.encode(self.GDINR, forKey: "GDINR");
        aCoder.encode(self.UserID, forKey: "UserID");
        aCoder.encode(self.FirstName, forKey: "FirstName");
        aCoder.encode(self.LastName, forKey: "LastName");
        aCoder.encode(self.AD, forKey: "AD");
        aCoder.encode(self.Kunde, forKey: "Kunde");
        aCoder.encode(self.isUserAdmin, forKey: "isUserAdmin");
        aCoder.encode(self.Datenschutz, forKey: "Datenschutz");
        aCoder.encode(self.AccessLevel, forKey: "AccessLevel");
        aCoder.encode(self.Kollegenr, forKey: "Kollegenr");
        aCoder.encode(self.UnitID, forKey: "UnitID");
        aCoder.encode(self.GDINRSPOT, forKey: "GDINRSPOT");
        aCoder.encode(self.UserNR, forKey: "UserNR");
        aCoder.encode(self.id_mitarbeiter, forKey: "id_mitarbeiter");
    }
    
    
    func fullName() -> String
    {
        var fullName = "";
        if let _ = self.FirstName , let _ = self.LastName
        {
            fullName = "\(self.FirstName!) \(self.LastName!)";
        }
        else if let _ = self.FirstName
        {
            fullName = self.FirstName!;
        }
        else if let _ = self.LastName
        {
            fullName = self.LastName!;
        }
        return fullName;
    }
    
}

//
//  AppSingleton.swift
//  UGW
//
//  Created by Qaiser Abbas on 05/02/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit
var sharedAppInstance : AppSingleton! = nil;

class AppSingleton: NSObject
{
    var AppUser: AppUser? = nil
    class func sharedUtility() -> AppSingleton
    {
        if sharedAppInstance == nil
        {
            sharedAppInstance = AppSingleton();
        }
        return sharedAppInstance;
    }

}

//
//  Constants.swift
//  UGW
//
//  Created by Qaiser Abbas on 31/01/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import Foundation
import CoreLocation

let App_BaseUrl              = "https://iosapp.ugw-showroom.de"

let Webservice_Login         = "www/service/login"
let Webservice_Callendar     = "www/service/data"
let Webservice_Available     = "www/service/available"
let Webservice_UnAvailable   = "www/service/un-available"
let Webservice_Accepted      = "www/service/accepted"
let Webservice_Cancel        = "www/service/cancel"


let AD                      = "AD"
let AccessLevel             = "AccessLevel"
let Active                  = "Active"
let Datenschutz             = "Datenschutz"
let FirstName               = "FirstName"
let GDINR                   = "GDINR"
let GDINRSPOT               = "GDINRSPOT"
let Kollegenr               = "Kollegenr"
let Kunde                   = "Kunde"
let LastName                = "LastName"
let UnitID                  = "UnitID"
let UserID                  = "UserID"
let UserNR                  = "UserNR"
let email                   = "email"
let id_mitarbeiter          = "id_mitarbeiter"
let isUserAdmin             = "isUserAdmin"

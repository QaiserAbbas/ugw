//
//  ViistDetailTableViewCell.swift
//  UGW
//
//  Created by Qaiser Abbas on 28/03/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit

class ViistDetailTableViewCell: UITableViewCell {

    @IBOutlet var jaButtonConstraint: NSLayoutConstraint!
    @IBOutlet var jaLabelConstraint: NSLayoutConstraint!
    @IBOutlet var neinButtonConstraint: NSLayoutConstraint!
    @IBOutlet var neinLabelConstraint: NSLayoutConstraint!
    @IBOutlet var zahlTexfieldConstraint: NSLayoutConstraint!
    @IBOutlet var priesTexfieldConstraint: NSLayoutConstraint!
    @IBOutlet var datumTexfieldConstraint: NSLayoutConstraint!
    @IBOutlet var zietTexfieldConstraint: NSLayoutConstraint!
    @IBOutlet var texfiledImageViewconstraint: NSLayoutConstraint!
    @IBOutlet var textViewconstraint: NSLayoutConstraint!
    @IBOutlet var textViewImageViewConstraint: NSLayoutConstraint!
    @IBOutlet var mcqImageViewTop: NSLayoutConstraint!
    
    
    @IBOutlet var frageInfoLabel: UILabel!
    @IBOutlet var jaButton: UIButton!
    @IBOutlet var jaLabel: UILabel!
    @IBOutlet var neinButton: UIButton!
    @IBOutlet var neinLabel: UILabel!
    @IBOutlet var jaNeinImageView: UIImageView!
    
    @IBOutlet var ZahlTexfield: UITextField!
    @IBOutlet var priestextField: UITextField!
    @IBOutlet var datumTextfield: UITextField!
    @IBOutlet var zietTextfield: UITextField!
    @IBOutlet var textfieldsImageview: UIImageView!
    
    @IBOutlet var detailtextView: UITextView!
    @IBOutlet var textViewImageview: UIImageView!
    
    @IBOutlet var checkButton1: UIButton!
    @IBOutlet var questionLabel1: UILabel!
    @IBOutlet var checkButton2: UIButton!
    @IBOutlet var questionLabel2: UILabel!
    @IBOutlet var checkButton3: UIButton!
    @IBOutlet var questionLabel3: UILabel!
    @IBOutlet var checkButton4: UIButton!
    @IBOutlet var questionLabel4: UILabel!
    
    @IBOutlet var separatorImageView: UIImageView!
    
    @IBOutlet var productTableView: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setcellData(visitData:NSDictionary) -> Void
    {
        print(visitData)
        
        if let val = visitData["antwortjanein"]
        {
            if val as! Int == 0
            {
                self.jaButton.isHidden = true
                self.jaLabel.isHidden = true
                self.neinButton.isHidden = true
                self.neinLabel.isHidden = true
                self.jaNeinImageView.isHidden = true
                self.zahlTexfieldConstraint.constant = -25
            }
        }
        else
        {
            self.jaButton.isHidden = true
            self.jaLabel.isHidden = true
            self.neinButton.isHidden = true
            self.neinLabel.isHidden = true
            self.jaNeinImageView.isHidden = true
            self.zahlTexfieldConstraint.constant = -25
        }
        var hide = true
        
        if let val = visitData["antwortzahl"]
        {
            if val as! Int == 0
            {
                self.ZahlTexfield.isHidden = true
                self.priesTexfieldConstraint.constant = -25
            }
            else
            {
                hide = false
            }
        }
        else
        {
            self.ZahlTexfield.isHidden = true
            self.priesTexfieldConstraint.constant = -25
        }
        
        if let val = visitData["antwortdbl"]
        {
            if val as! Int == 0
            {
                self.priestextField.isHidden = true
                self.datumTexfieldConstraint.constant = -25
            }
            else
            {
                hide = false
            }
        }
        else
        {
            self.priestextField.isHidden = true
            self.datumTexfieldConstraint.constant = -25
        }
        if let val = visitData["antwortdate"]
        {
            if val as! Int == 0
            {
                self.datumTextfield.isHidden = true
                self.zietTexfieldConstraint.constant = -25
            }
            else
            {
                hide = false
            }
        }
        else
        {
            self.datumTextfield.isHidden = true
            self.zietTexfieldConstraint.constant = -25
        }
        if let val = visitData["antwortzeit"]
        {
            if val as! Int == 0
            {
                self.zietTextfield.isHidden = true
                self.texfiledImageViewconstraint.constant = -25
            }
            else
            {
                hide = false
            }
        }
        else
        {
            self.zietTextfield.isHidden = true
            self.texfiledImageViewconstraint.constant = -25
        }
        if hide
        {
            self.textfieldsImageview.isHidden = true
            self.textViewconstraint.constant = -25
        }
        
        self.checkButton1.isHidden = true
        self.checkButton2.isHidden = true
        self.checkButton3.isHidden = true
        self.checkButton4.isHidden = true
        
        self.questionLabel1.isHidden = true
        self.questionLabel2.isHidden = true
        self.questionLabel3.isHidden = true
        self.questionLabel4.isHidden = true
        
        if let val = visitData["antwortauswahl"]
        {
            if val as! Int == 0
            {
                self.checkButton1.isHidden = true
                self.checkButton2.isHidden = true
                self.checkButton3.isHidden = true
                self.checkButton4.isHidden = true
                
                self.questionLabel1.isHidden = true
                self.questionLabel2.isHidden = true
                self.questionLabel3.isHidden = true
                self.questionLabel4.isHidden = true
                
                self.mcqImageViewTop.constant = -140
            }
            else
            {
                let mcqArray = visitData["mcqOptions"] as! NSArray
                
                if mcqArray.count == 1
                {
                    self.checkButton1.isHidden = false
                    
                    self.questionLabel1.isHidden = false
                    self.mcqImageViewTop.constant = -105
                    
                }
                else if mcqArray.count == 2
                {
                    self.checkButton1.isHidden = false
                    self.checkButton2.isHidden = false
                    
                    self.questionLabel1.isHidden = false
                    self.questionLabel2.isHidden = false
                    self.mcqImageViewTop.constant = -70
                    
                }
                else if mcqArray.count == 3
                {
                    self.checkButton1.isHidden = false
                    self.checkButton2.isHidden = false
                    self.checkButton3.isHidden = false
                    
                    self.questionLabel1.isHidden = false
                    self.questionLabel2.isHidden = false
                    self.questionLabel3.isHidden = false
                    self.mcqImageViewTop.constant = -35
                    
                }
                else if mcqArray.count == 4
                {
                    self.checkButton1.isHidden = false
                    self.checkButton2.isHidden = false
                    self.checkButton3.isHidden = false
                    self.checkButton4.isHidden = false
                    
                    self.questionLabel1.isHidden = false
                    self.questionLabel2.isHidden = false
                    self.questionLabel3.isHidden = false
                    self.questionLabel4.isHidden = false
                    self.mcqImageViewTop.constant = 5
                }
                
                
            }
        }
        else
        {
            self.checkButton1.isHidden = true
            self.checkButton2.isHidden = true
            self.checkButton3.isHidden = true
            self.checkButton4.isHidden = true
            
            self.questionLabel1.isHidden = true
            self.questionLabel2.isHidden = true
            self.questionLabel3.isHidden = true
            self.questionLabel4.isHidden = true
        }

    }
}

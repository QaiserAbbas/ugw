//
//  VisitDetailViewController.swift
//  UGW
//
//  Created by Qaiser Abbas on 04/03/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit
import CoreLocation

class VisitDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var dataDict : NSDictionary = NSDictionary()
    var dataDictVisit : NSDictionary? = NSDictionary()
    var dataDictStudy : NSDictionary? = NSDictionary()
    var studyInfo : NSMutableArray? = NSMutableArray()
    var appUser : AppUser! = nil;
    let cellReuseIdentifier = "cell"
    
    var einsatzberichtData : NSMutableArray? = NSMutableArray()

    var slectedTab = "information"
    
    @IBOutlet var mapViewReference: GMSMapView!
    @IBOutlet var bottomDetailView: UIView!
    @IBOutlet var moreDetailView: UIView!

    @IBOutlet var Outletnr: UILabel!
    @IBOutlet var Bescuhsnr: UILabel!
    @IBOutlet var Ankunft: UILabel!
    @IBOutlet var Abfahrt: UILabel!
    
    @IBOutlet var dateLeftLabel: UILabel!
    @IBOutlet var dateRightLabel: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var infoLabel: UILabel!
    @IBOutlet var hashLabel: UILabel!
    
    @IBOutlet weak var visitTableView: UITableView!
    @IBOutlet var einsatzberichtButton: UIButton!
    @IBOutlet var informationButton: UIButton!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        appUser = AppSingleton.sharedUtility().AppUser
        self.visitTableView.register(UINib(nibName: "infoTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        self.visitTableView.register(UINib(nibName: "ViistDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "cell2")

        visitTableView.delegate = self
        visitTableView.dataSource = self

        self.getVisitInfo()
        self.loadGooglrMap()
        self.manageView()
        self.getStudyInfo()
    }
    
    func getVisitInfo()
    {
    let visitInfoArray = ModelManager.getInstance().fetchVisitinformation(tabelName: dataDict["table"] as! String, besuchsnr:dataDict["besuchsnr"] as! Int?)
        if visitInfoArray.count>0
        {
            dataDictVisit = visitInfoArray[0] as? NSDictionary
        }
    }
    
    func getStudyInfo()
    {
        let visitInfoArray = ModelManager.getInstance().fetchStudyinformation(tabelName: dataDict["table"] as! String, studienr:dataDictVisit?["studienr"] as! Int?)
        if visitInfoArray.count>0
        {
            dataDictStudy = visitInfoArray[0] as? NSDictionary
        }
//        print(dataDictStudy)

        for i in 1...9
        {
            let value = dataDictStudy?["besuch_c\(i)"] as? String
            if (value != nil)
            {
                if (value?.isEmpty)!
                {
                    
                }
                else
                {
                    studyInfo?.add(value ?? "")
                }
            }
        }
        
        for i in 1...9
        {
            let value = dataDictStudy?["besuch_i\(i)"] as? String
            if (value != nil)
            {
                if (value?.isEmpty)!
                {
                    
                }
                else
                {
                    studyInfo?.add(value ?? "")
                }
            }
        }
        self.visitTableView.reloadData()
        
//        print(studyInfo)
    }
    
    func manageView()
    {
        
        Outletnr.text = "\(dataDict["outletnr"]!)"
        Bescuhsnr.text = "\(dataDict["besuchsnr"]!)"
        Ankunft.text = "\(dataDict["ankunft"]!)"
        Abfahrt.text = "\(dataDict["abfahrt"]!)"
        
        dateLeftLabel.text = "\(dataDict["besuchsdatum"]!)"
        dateRightLabel.text = ""
        userName.text = appUser.fullName()
        infoLabel.text = "\(dataDict["statuskurz"]!)"
        hashLabel.text = "\(dataDict["tournr"]!)"
   }
    
    func loadGooglrMap()
    {
        let address = "\(dataDict["o_name"]!), \(dataDict["o_strasse"]!), \(dataDict["o_ort"]!)"
        let geocoder = CLGeocoder()
        
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error ?? "No error :P")
            }
            if let placemark = placemarks?.first {
                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
//                print(coordinates);
                let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 6)
                self.mapViewReference.isMyLocationEnabled = true
                
                self.mapViewReference.camera = camera

                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
                marker.title = self.dataDict["o_name"] as? String
                marker.snippet = address
                marker.map = self.mapViewReference
                self.mapViewReference.selectedMarker=marker
            }
        })

        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func moreDetailButtonTap(_ sender: UIButton)
    {
        moreDetailView.isHidden = false
        
    }
    
    @IBAction func backButtonTap(_ sender: UIButton)
    {
        if let navController = self.navigationController
        {
            navController.popViewController(animated: true)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if slectedTab == "information"
        {
            return 1
        }
        else if slectedTab == "einsatzbericht"
        {
            print((einsatzberichtData?.count)!)
            
            return (einsatzberichtData?.count)!
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if slectedTab == "information"
        {
            return (studyInfo?.count)!
        }
        else if slectedTab == "einsatzbericht"
        {
            let dataDict = einsatzberichtData![section] as! NSDictionary
            
            return (dataDict["innerSections"]! as AnyObject).count
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if slectedTab == "information"
        {
            let cell:infoTableViewCell = self.visitTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! infoTableViewCell
            let dataDict = studyInfo?[indexPath.row] as! String
            cell.infoCellLabel.text = dataDict
            return cell
        }
        else if slectedTab == "einsatzbericht"
        {
            let cell:ViistDetailTableViewCell = self.visitTableView.dequeueReusableCell(withIdentifier: "cell2") as! ViistDetailTableViewCell
            
            cell.jaButton.isHidden = false
            cell.jaLabel.isHidden = false
            cell.neinButton.isHidden = false
            cell.neinLabel.isHidden = false
            cell.jaNeinImageView.isHidden = false
            cell.zahlTexfieldConstraint.constant = 5

            cell.ZahlTexfield.isHidden = false
            cell.priesTexfieldConstraint.constant = 5

            cell.priestextField.isHidden = false
            cell.datumTexfieldConstraint.constant = 5

            cell.datumTextfield.isHidden = false
            cell.zietTexfieldConstraint.constant = 5

            cell.textfieldsImageview.isHidden = false
            cell.textViewconstraint.constant = 5

            let dataDict = einsatzberichtData![indexPath.section] as! NSDictionary
            
            if dataDict["block_type"] as! String == "p"
            {
                cell.productTableView.isHidden = false
                return cell
            }
            else
            {
                cell.productTableView.isHidden = true

            }
            let dataDict2 = dataDict["innerSections"] as! NSArray
            print(dataDict)
            
            if dataDict2.count>0
            {
                let visitData = dataDict2[indexPath.row] as! NSDictionary

                cell.setcellData(visitData: visitData)
                print(visitData)
            }

            return cell

        }
        let cell:infoTableViewCell = self.visitTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! infoTableViewCell
        let dataDict = studyInfo?[indexPath.row] as! String
        cell.infoCellLabel.text = dataDict
        return cell
        // create a new cell if needed or reuse an old one
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 25))
        returnedView.backgroundColor = .blue
        
        return returnedView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if slectedTab == "information"
        {
            return 0
        }
        else if slectedTab == "einsatzbericht"
        {
            return 30
        }
        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if slectedTab == "information"
        {
            return 45
        }
        else if slectedTab == "einsatzbericht"
        {
            let dataDict = einsatzberichtData![indexPath.section] as! NSDictionary
            if dataDict["block_type"] as! String == "p"
            {
                return 506
            }

            let dataDict2 = dataDict["innerSections"] as! NSArray
            let visitData = dataDict2[indexPath.row] as! NSDictionary
            print(visitData)
            
            return self.calculateHeightOfRow(dataDict: visitData)
        }
        
        return 0
    }
    @IBAction func backOndetailViewtap(_ sender: UIButton)
    {
        moreDetailView.isHidden = true
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 62
//    }
    @IBAction func informationButtonTap(_ sender: UIButton)
    {
        slectedTab = "information"
        visitTableView.reloadData()
    }

    @IBAction func einsatzberichtButtonTap(_ sender: UIButton)
    {
        let visitInfoArray = ModelManager.getInstance().fetchBlockTypeandNumbr(tabelName: dataDict["table"] as! String, studienr:dataDict["studienr"] as! Int?)

        var innerTable = "s_fragestudie"
        
        einsatzberichtData = NSMutableArray()
        for dataDictionary  in visitInfoArray
        {
            let mutableNsDict = dataDictionary as! NSDictionary
            let myDict = mutableNsDict.mutableCopy() as! NSMutableDictionary

            if myDict["block_type"]! as! String == "f"
            {
                innerTable = "s_fragestudie"
            }
            else
            {
                innerTable = "s_produktstudie"
            }
            if !(myDict["block_number"] as? Int != nil)
            {
                myDict["block_number"] = 0
            }

            let visitInfoArrayNew = ModelManager.getInstance().fetchDataOfBlockType(tabelName: dataDict["table"] as! String, studienr: dataDict["studienr"] as! Int?, innerName: innerTable, pagenr: myDict["block_number"] as? Int,comesFrom: myDict["block_type"]! as! String)
            print(visitInfoArrayNew);
            let visitInfoArrayUpdated : NSMutableArray? = NSMutableArray()

            if myDict["block_type"]! as! String == "f"
            {
                for mcqDict in visitInfoArrayNew
                {
                    let mcqDictionary = mcqDict as! NSDictionary
                    let mcqDictionaryMutable = mcqDictionary.mutableCopy() as! NSMutableDictionary
                    
                    if let val = mcqDictionaryMutable["antwortauswahl"]
                    {
                        if val as! Int == 1
                        {
                            let visitInfoArrayNew = ModelManager.getInstance().fetchMCQs(tabelName: dataDict["table"] as! String, studienr: mcqDictionaryMutable["studienr"] as! Int?, innerName: "s_bwertung", fragenr: mcqDictionaryMutable["fragenr"] as? Int)
                            print(visitInfoArrayNew)
                            mcqDictionaryMutable["mcqOptions"] = visitInfoArrayNew
                        }
                    }
                    visitInfoArrayUpdated?.add(mcqDictionaryMutable)
                }
                myDict["innerSections"] = visitInfoArrayUpdated
                einsatzberichtData?.add(myDict)
            }
            else
            {
                for mcqDict in visitInfoArrayNew
                {
                    let mcqDictionary = mcqDict as! NSDictionary
                    let mcqDictionaryMutable = mcqDictionary.mutableCopy() as! NSMutableDictionary
                    print(mcqDictionaryMutable)
                    let visitInfoArrayNew = ModelManager.getInstance().fetchColumns(tabelName: dataDict["table"] as! String, studienr: mcqDictionaryMutable["studienr"] as! Int?, innerName: "s_produktdef", pagenr: mcqDictionaryMutable["pagenr"] as? Int)

                            print(visitInfoArrayNew)
                            mcqDictionaryMutable["columns"] = visitInfoArrayNew
                    visitInfoArrayUpdated?.add(mcqDictionaryMutable)
                }
                myDict["innerSections"] = visitInfoArrayUpdated
                einsatzberichtData?.add(myDict)
            }
        }
        
        print(einsatzberichtData ?? 0)
        slectedTab = "einsatzbericht"
        visitTableView.reloadData()

    }
    func calculateHeightOfRow(dataDict: NSDictionary) -> CGFloat
    {
        var numberOfitems = 0
        
        if let val = dataDict["antwortjanein"]
        {
            if val as! Int == 1
            {
                numberOfitems += 1
            }
        }
        
        if let val = dataDict["antwortzahl"]
        {
            if val as! Int == 1
            {
                numberOfitems += 1
            }
        }

        if let val = dataDict["antwortdbl"]
        {
            if val as! Int == 1
            {
                numberOfitems += 1
            }
        }
        if let val = dataDict["antwortdate"]
        {
            if val as! Int == 1
            {
                numberOfitems += 1
            }
        }
        if let val = dataDict["antwortzeit"]
        {
            if val as! Int == 1
            {
                numberOfitems += 1
            }
        }
        var mcqHeight = 0
        if let val = dataDict["antwortauswahl"]
        {
            if val as! Int == 0
            {
                //                        cell.priesTexfieldConstraint.constant = -25
                mcqHeight = 0
            }
            else
            {
                let mcqArray = dataDict["mcqOptions"] as! NSArray
                
                if mcqArray.count == 1
                {
                    mcqHeight = 50
                }
                else if mcqArray.count == 2
                {
                    mcqHeight = 100
                }
                else if mcqArray.count == 3
                {
                    mcqHeight = 150
                }
                else if mcqArray.count == 4
                {
                    mcqHeight = 200
                }
            }
        }
        else
        {
           mcqHeight = 0
        }
        let totalHeight = (numberOfitems*30)+180+mcqHeight
        return CGFloat(totalHeight)
    }
    
    }

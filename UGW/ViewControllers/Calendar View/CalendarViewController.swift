//
//  CalendarViewController.swift
//  UGW
//
//  Created by Qaiser Abbas on 31/01/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit

class CalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var calendarCustom: FSCalendar!
    @IBOutlet weak var calendarHeightConstraint: NSLayoutConstraint!
    let cellReuseIdentifier = "cell"

    @IBOutlet weak var visitTableView: UITableView!
    @IBOutlet weak var tableUpperConstraint: NSLayoutConstraint!
    private let gregorian = Calendar(identifier: .gregorian)
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()

    private weak var eventLabel: UILabel!

    
    var calendarData: NSMutableArray = []
    var VisitData: NSMutableArray = []
    var selectedVisitData: NSMutableArray = []

    var selectedDatesLocal = [NSDate]()
    var selectedTypes = [NSString]()
    var appUser : AppUser! = nil;
    
    @IBOutlet var availableButton: UIButton!
    @IBOutlet var notAvailableButton: UIButton!
    @IBOutlet var acceptedButton: UIButton!
    @IBOutlet var cancelledAllButton: UIButton!
    
    @IBOutlet weak var mainPopUpView: UIView!
    @IBOutlet weak var PopUpCantCancel: UIView!
    @IBOutlet weak var addCommentView: UIView!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        appUser = AppSingleton.sharedUtility().AppUser
        
        self.availableButton?.layer.cornerRadius = 20
        self.availableButton?.clipsToBounds = true

        notAvailableButton?.layer.cornerRadius = 20
        notAvailableButton?.clipsToBounds = true

        cancelledAllButton?.layer.cornerRadius = 20
        cancelledAllButton?.clipsToBounds = true

        acceptedButton?.layer.cornerRadius = 20
        acceptedButton?.clipsToBounds = true

        
        self.submitButton.layer.borderWidth = 1.0
        self.submitButton.layer.borderColor = UIColor.gray.cgColor
        submitButton?.layer.cornerRadius = 5
        submitButton?.clipsToBounds = true

        self.backButton.layer.borderWidth = 1.0
        self.backButton.layer.borderColor = UIColor.gray.cgColor
        backButton?.layer.cornerRadius = 5
        backButton?.clipsToBounds = true
        
        self.commentTextView.layer.borderWidth = 1.0
        self.commentTextView.layer.borderColor = UIColor.gray.cgColor
        commentTextView?.layer.cornerRadius = 10
        commentTextView?.clipsToBounds = true


        enableDisableButtons()

        calendarData = ModelManager.getInstance().getAllCalendarData()
        VisitData = ModelManager.getInstance().getAllVisitData()
        
        self.visitTableView.register(UINib(nibName: "VisitTableViewCell", bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        
        visitTableView.delegate = self
        visitTableView.dataSource = self

        self.calendarCustom.dataSource = self
        self.calendarCustom.delegate = self
        self.calendarCustom.allowsMultipleSelection = true
        view.addSubview(self.calendarCustom)
        
        calendarCustom.calendarHeaderView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        calendarCustom.calendarWeekdayView.backgroundColor = UIColor.white.withAlphaComponent(0.1)
        calendarCustom.appearance.eventSelectionColor = UIColor.white
        calendarCustom.appearance.eventOffset = CGPoint(x: 0, y: -7)
        calendarCustom.today = nil // Hide the today circle
        calendarCustom.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        calendarCustom.clipsToBounds = true // Remove top/bottom line
        
        calendarCustom.swipeToChooseGesture.isEnabled = true // Swipe-To-Choose
        
        let scopeGesture = UIPanGestureRecognizer(target: calendarCustom, action: #selector(calendarCustom.handleScopeGesture(_:)));
        calendarCustom.addGestureRecognizer(scopeGesture)
        let panGesture = UIPanGestureRecognizer(target: self, action:#selector(myPanAction))
        calendarCustom.addGestureRecognizer(panGesture)

        
        let label = UILabel(frame: CGRect(x: 0, y: calendarCustom.frame.maxY + 10, width: self.view.frame.size.width, height: 50))
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .subheadline)
        self.view.addSubview(label)
        self.eventLabel = label

        // Do any additional setup after loading the view.
        if UIDevice.current.model.hasPrefix("iPad") {
            self.calendarHeightConstraint.constant = 400
        }

    }

    deinit
    {
        print("\(#function)")
    }
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell
    {
        let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
        let day: String! = self.formatter.string(from: date)
        let speciesPredicate = NSPredicate(format: "arbeitsdatum == %@", day)
        let filteredArray = (calendarData as NSMutableArray).filtered(using: speciesPredicate)
        if filteredArray.count > 0
        {
            let dataDict = filteredArray[0] as! NSDictionary
//            print(type(of:dataDict))
//            print(dataDict["arbeitstatus"]!)
//            print(dataDict["arbeitstatus"]!)
//            
            if dataDict["arbeitstatus"]! as! String == "A" && (dataDict["z_confirmed"] != nil)
            {
                cell.backgroundColor =  UIColor.green.withAlphaComponent(0.12)
            }
            else
            {
                if dataDict["arbeitstatus"]! as! String == "C"
                {
                    cell.backgroundColor =  UIColor.orange.withAlphaComponent(0.12)
                }
                else if dataDict["arbeitstatus"]! as! String == "0"
                {
                    cell.backgroundColor =  UIColor.white.withAlphaComponent(0.12)
                }
                else
                {
                    cell.backgroundColor =  UIColor.yellow.withAlphaComponent(0.12)
                }
            }
        }
        else
        {
            cell.backgroundColor =  UIColor.lightGray.withAlphaComponent(0.12)
        }

        return cell
    }
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition)
    {
        configure(cell: cell, for: date, at: position)
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition)   -> Bool
    {
        return monthPosition == .current;
    }
    
    //    func minimumDate(for calendar: FSCalendar) -> Date {
    //        return Date()
    //    }
    //
    //    func maximumDate(for calendar: FSCalendar) -> Date {
    //        let oneYearFromNow = self.gregorian.date(byAdding: .year, value: 1, to: Date(), wrappingComponents: true)
    //        return oneYearFromNow!
    //    }
    
    func calendar(_ calendar: FSCalendar, titleFor date: Date) -> String?
    {
        if self.gregorian.isDateInToday(date) {
            return "今"
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int
    {
        let day: String! = self.formatter.string(from: date)
//        print(day)
        
        let speciesPredicate = NSPredicate(format: "besuchsdatum == %@", day)
        
        let filteredArray = (VisitData as NSMutableArray).filtered(using: speciesPredicate)
        
        return filteredArray.count;
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool)
    {
        self.calendarCustom.frame.size.height = bounds.height
        self.eventLabel.frame.origin.y = calendar.frame.maxY + 10
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date)
    {
//        print("did select date \(self.formatter.string(from: date))")
        selectedDatesLocal.append(date as NSDate)
//        print(selectedDatesLocal)
        addType(date: date, remove: false)
//        print(selectedTypes)
        enableDisableButtons()
        self.configureVisibleCells()
        selectedVisitData = self.getVisitsOnSelectedDates()
        self.visitTableView.reloadData()

    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date) {
//        print("did deselect date \(self.formatter.string(from: date))")
        selectedDatesLocal = selectedDatesLocal.filter(){$0 as Date != date}
//        print(selectedDatesLocal)
        addType(date: date, remove: true)
//        print(selectedTypes)
        enableDisableButtons()
        self.configureVisibleCells()
        selectedVisitData = self.getVisitsOnSelectedDates()
        self.visitTableView.reloadData()
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        if self.gregorian.isDateInToday(date) {
            return [UIColor.orange]
        }
        return [appearance.eventDefaultColor]
    }
    
    // MARK: - Private functions
    
    private func configureVisibleCells()
    {
        calendarCustom.visibleCells().forEach { (cell) in
            let date = calendarCustom.date(for: cell)
            let position = calendarCustom.monthPosition(for: cell)
            self.configure(cell: cell, for: date, at: position)
        }
    }
    
    func getVisitsOnSelectedDates() -> NSMutableArray
    {
        let marrStudentInfo : NSMutableArray = NSMutableArray()
        for date in selectedDatesLocal
        {
            let day: String! = self.formatter.string(from: date as Date)
//            print(day)
            
            let speciesPredicate = NSPredicate(format: "besuchsdatum == %@", day)
            
            let filteredArray = (VisitData as NSMutableArray).filtered(using: speciesPredicate)
            marrStudentInfo.addObjects(from: filteredArray)
        }
        return marrStudentInfo
    }
    
    private func configure(cell: FSCalendarCell, for date: Date, at position: FSCalendarMonthPosition) {
        
        let diyCell = (cell as! DIYCalendarCell)
        // Custom today circle
        diyCell.circleImageView.isHidden = !self.gregorian.isDateInToday(date)
        // Configure selection layer
        if position == .current || calendarCustom.scope == .week {
            
            diyCell.eventIndicator.isHidden = false
            
            var selectionType = SelectionType.none
            
            if calendarCustom.selectedDates.contains(date) {
                let previousDate = self.gregorian.date(byAdding: .day, value: -1, to: date)!
                let nextDate = self.gregorian.date(byAdding: .day, value: 1, to: date)!
                if calendarCustom.selectedDates.contains(date) {
                    if calendarCustom.selectedDates.contains(previousDate) && calendarCustom.selectedDates.contains(nextDate) {
                        selectionType = .middle
                    }
                    else if calendarCustom.selectedDates.contains(previousDate) && calendarCustom.selectedDates.contains(date) {
                        selectionType = .rightBorder
                    }
                    else if calendarCustom.selectedDates.contains(nextDate) {
                        selectionType = .leftBorder
                    }
                    else {
                        selectionType = .single
                    }
                }
            }
            else {
                selectionType = .none
            }
            if selectionType == .none {
                diyCell.selectionLayer.isHidden = true
                return
            }
            
            diyCell.selectionLayer.isHidden = false
            diyCell.selectionType = selectionType
            
        }
        else if position == .next || position == .previous {
            diyCell.circleImageView.isHidden = true
            diyCell.selectionLayer.isHidden = true
            diyCell.eventIndicator.isHidden = true
            // Hide default event indicator
            if self.calendarCustom.selectedDates.contains(date) {
                diyCell.titleLabel!.textColor = self.calendarCustom.appearance.titlePlaceholderColor
                // Prevent placeholders from changing text color
            }
        }
    }
    func addType(date: Date, remove:Bool)
    {
        let day: String! = self.formatter.string(from: date)
        let speciesPredicate = NSPredicate(format: "arbeitsdatum == %@", day)
        let filteredArray = (calendarData as NSMutableArray).filtered(using: speciesPredicate)
        if filteredArray.count > 0
        {
            let dataDict = filteredArray[0] as! NSDictionary
//            print(type(of:dataDict))
//            print(dataDict["arbeitstatus"]!)
            
            if dataDict["arbeitstatus"]! as! String == "A"
            {
                if (dataDict["z_confirmed"] == nil)
                {
                    let day: String! = self.formatter.string(from: date)
//                    print(day)
                    
                    let speciesPredicate = NSPredicate(format: "besuchsdatum == %@", day)
                    
                    let filteredArray = (VisitData as NSMutableArray).filtered(using: speciesPredicate)

                    if filteredArray.count > 0
                    {
                        if !remove
                        {
                            selectedTypes.append("A")
                        }
                        else
                        {
                            if let index = selectedTypes.index(of: "A") {
                                selectedTypes.remove(at: index)
                            }
                        }
                    }
                    else
                    {
                        if !remove
                        {
                            selectedTypes.append("A2")
                        }
                        else
                        {
                            if let index = selectedTypes.index(of: "A2") {
                                selectedTypes.remove(at: index)
                            }
                        }
                    }

                }
                else
                {
                    if !remove
                    {
                        selectedTypes.append("A3")
                    }
                    else
                    {
                        if let index = selectedTypes.index(of: "A3") {
                            selectedTypes.remove(at: index)
                        }
                    }

                }
            }
            else if dataDict["arbeitstatus"]! as! String == "C"
            {
                let day: String! = self.formatter.string(from: date)
//                print(day)
                
                let speciesPredicate = NSPredicate(format: "besuchsdatum == %@", day)
                
                let filteredArray = (VisitData as NSMutableArray).filtered(using: speciesPredicate)
                var orangeStr = "C1"
                if filteredArray.count > 0
                {
                    orangeStr = "C2"
                }

                
                if !remove
                {
                    selectedTypes.append("\(orangeStr)" as NSString)
                }
                else
                {
                    if let index = selectedTypes.index(of: "\(orangeStr)" as NSString) {
                        selectedTypes.remove(at: index)
                    }
                }
            }
            else if dataDict["arbeitstatus"]! as! String == "0"
            {
                if !remove
                {
                    selectedTypes.append("0")
                }
                else
                {
                    if let index = selectedTypes.index(of: "0") {
                        selectedTypes.remove(at: index)
                    }
                }
            }
        }
        else
        {
            if !remove
            {
                selectedTypes.append("0")
            }
            else
            {
                if let index = selectedTypes.index(of: "0") {
                    selectedTypes.remove(at: index)
                }
            }
        }
    }
    
    func enableDisableButtons()
    {
        availableButton.isEnabled = false
        notAvailableButton.isEnabled = false
        cancelledAllButton.isEnabled = false
        acceptedButton.isEnabled = false
        availableButton.alpha = 0.3
        notAvailableButton.alpha = 0.3
        cancelledAllButton.alpha = 0.3
        acceptedButton.alpha = 0.3

        if selectedTypes.contains("A1")
        {
            cancelledAllButton.isEnabled = true
            cancelledAllButton.alpha = 1
        }
        if selectedTypes.contains("0")
        {
            availableButton.isEnabled = true
            availableButton.alpha = 1
        }
        if selectedTypes.contains("A2")
        {
            notAvailableButton.isEnabled = true
            notAvailableButton.alpha = 1
        }
        if selectedTypes.contains("A3")
        {
            cancelledAllButton.isEnabled = true
            cancelledAllButton.alpha = 1
        }

        if selectedTypes.contains("A")
        {
            acceptedButton.isEnabled = true
            acceptedButton.alpha = 1
            cancelledAllButton.isEnabled = true
            cancelledAllButton.alpha = 1
        }

    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func availableButtonTap(_ sender: UIButton)
    {
        var datStr: String = ""
        for date in selectedDatesLocal
        {
            let day: String! = self.formatter.string(from: date as Date)
            datStr += "\(day!),"
        }
//        print(datStr)
        
        let truncated = datStr.substring(to: datStr.index(before: datStr.endIndex))
//        print(truncated)
        BackendBase.ShowMBProgress(view: self.view)
        
                let paramsData: [String : AnyObject] = ["kollegenr": appUser.Kollegenr as AnyObject, "day":truncated as AnyObject]
                
                BackendBase.accessApiByPost(apiPath: App_BaseUrl + "/" + Webservice_Available, parameters: paramsData, Success: { (result) in
//                    print(result)
                    for date in self.selectedDatesLocal
                    {
                        self.calendarCustom.deselect(date as Date)
                        self.selectedDatesLocal = self.selectedDatesLocal.filter(){$0 as Date != date as Date}
                        self.addType(date: date as Date, remove: true)
                        self.enableDisableButtons()
                        self.configureVisibleCells()
                        self.selectedVisitData = self.getVisitsOnSelectedDates()
                        self.visitTableView.reloadData()

                        let day: String! = self.formatter.string(from: date as Date)

                        let dataDict = self.getStatusOnSelectedDate(date)
//                        print(dataDict)
                        if dataDict["arbeitstatus"]! as! String == "0"
                        {
                            ModelManager.getInstance().updateAvailabilityForAvailable("A", date: day, dateToUpdate: date as Date)
                        }
                    }
                    self.calendarData.removeAllObjects()
                    self.calendarData = ModelManager.getInstance().getAllCalendarData()

                    self.calendarCustom.reloadData()
                    BackendBase.HideMBProgress(view: self.view)
                    
                }, Failure: { (error) in
                    print(error)
                    BackendBase.HideMBProgress(view: self.view)
                    
                })
    }
    
    @IBAction func notAvailableButtonTap(_ sender: UIButton)
    {
        var datStr: String = ""
        for date in selectedDatesLocal
        {
            let day: String! = self.formatter.string(from: date as Date)
            datStr += "\(day!),"
        }
        var truncated = ""
//        print(datStr)
        if datStr.isEmpty
        {
            
        }
        else
        {
            truncated = datStr.substring(to: datStr.index(before: datStr.endIndex))
        }
        
//        print(truncated)
        BackendBase.ShowMBProgress(view: self.view)
        
        let paramsData: [String : AnyObject] = ["kollegenr": appUser.Kollegenr as AnyObject, "day":truncated as AnyObject]
        
        BackendBase.accessApiByPost(apiPath: App_BaseUrl + "/" + Webservice_UnAvailable, parameters: paramsData, Success: { (result) in
//            print(result)
            for date in self.selectedDatesLocal
            {
                self.calendarCustom.deselect(date as Date)
                self.selectedDatesLocal = self.selectedDatesLocal.filter(){$0 as Date != date as Date}
                self.addType(date: date as Date, remove: true)
                self.enableDisableButtons()
                self.configureVisibleCells()
                self.selectedVisitData = self.getVisitsOnSelectedDates()
                self.visitTableView.reloadData()

                let day: String! = self.formatter.string(from: date as Date)
                
                let dataDict = self.getStatusOnSelectedDate(date)
//                print(dataDict)
                if dataDict["arbeitstatus"]! as! String == "A"
                {
                    ModelManager.getInstance().updateAvailabilityForAvailable("0", date: day, dateToUpdate: date as Date)
                }
            }
            self.calendarData.removeAllObjects()
            self.calendarData = ModelManager.getInstance().getAllCalendarData()
//
//            self.calendarCustom.reloadData()

            BackendBase.HideMBProgress(view: self.view)
            
        }, Failure: { (error) in
            print(error)
            BackendBase.HideMBProgress(view: self.view)
            
        })

    }
    
    @IBAction func acceptedButtonTap(_ sender: UIButton)
    {
        var datStr: String = ""
        for date in selectedDatesLocal
        {
            let day: String! = self.formatter.string(from: date as Date)
            datStr += "\(day!),"
        }
//        print(datStr)
        
        let truncated = datStr.substring(to: datStr.index(before: datStr.endIndex))
//        print(truncated)
        BackendBase.ShowMBProgress(view: self.view)
        
        let paramsData: [String : AnyObject] = ["kollegenr": appUser.Kollegenr as AnyObject, "day":truncated as AnyObject]
        
        BackendBase.accessApiByPost(apiPath: App_BaseUrl + "/" + Webservice_Accepted, parameters: paramsData, Success: { (result) in
//            print(result)
            for date in self.selectedDatesLocal
            {
                self.calendarCustom.deselect(date as Date)
                self.selectedDatesLocal = self.selectedDatesLocal.filter(){$0 as Date != date as Date}
                self.addType(date: date as Date, remove: true)
                self.enableDisableButtons()
                self.configureVisibleCells()
                self.selectedVisitData = self.getVisitsOnSelectedDates()
                self.visitTableView.reloadData()
                
                let day: String! = self.formatter.string(from: date as Date)
                
                let dataDict = self.getStatusOnSelectedDate(date)
//                print(dataDict)
                if dataDict["arbeitstatus"]! as! String == "A"
                {
                    ModelManager.getInstance().updateAvailabilityForAccepted("A", date: day, dateToUpdate: date as Date)
                }
            }
            self.calendarData.removeAllObjects()
            self.calendarData = ModelManager.getInstance().getAllCalendarData()
            //
            //            self.calendarCustom.reloadData()

            BackendBase.HideMBProgress(view: self.view)
            
        }, Failure: { (error) in
            print(error)
            BackendBase.HideMBProgress(view: self.view)
            
        })

    }
    
    @IBAction func cancelAllButtonTap(_ sender: UIButton)
    {
        var canCancel = true
        for myDate in selectedDatesLocal
        {
           let difference = self.daysBetween(start: myDate as Date, end: NSDate() as Date)
            if difference >= 5 || difference <= -5
            {
                canCancel = false
            }
            else
            {
                canCancel = true
                break
            }
        }
        mainPopUpView.isHidden = false
        self.view.bringSubview(toFront: mainPopUpView)
        if canCancel
        {
            self.PopUpCantCancel.isHidden = false
        }
        else
        {
            self.addCommentView.isHidden = false
        }
    }
    
    func daysBetween(start: Date, end: Date) -> Int
    {
        return Calendar.current.dateComponents([.day], from: start, to: end).day!
    }
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer)
    {
        
        
        //        get translation
        
//        var translation = panGesture.translationInView(view)
//        panGesture.setTranslation(CGPointZero, inView: view)
//        println(translation)
//        
//        
//        //create a new Label and give it the parameters of the old one
//        var label = panGesture.view as UIImageView
//        label.center = CGPoint(x: label.center.x+translation.x, y: label.center.y+translation.y)
//        label.multipleTouchEnabled = true
//        label.userInteractionEnabled = true
//        
//        if panGesture.state == UIGestureRecognizerState.Began {
//            
//            //add something you want to happen when the Label Panning has started
//        }
//        
//        if panGesture.state == UIGestureRecognizerState.Ended {
//            
//            //add something you want to happen when the Label Panning has ended
//            
//        }
//        
//        
//        if panGesture.state == UIGestureRecognizerState.Changed {
//            
//            //add something you want to happen when the Label Panning has been change ( during the moving/panning )
//            
//        }
//            
//        else {
//            
//            // or something when its not moving
//        }
    }
    
    func myPanAction(recognizer: UIPanGestureRecognizer)
    {
            calendarCustom.handleScopeGesture(_:recognizer)
            
            let translation = recognizer.translation(in: self.view)
            
            // Adjust mapView height
            if translation.y > 0
            {
                
            }
//            tableUpperConstraint.constant += translation.y

//            var mapViewFrame = visitTableView.frame
//            mapViewFrame.origin.y -= translation.y
//            visitTableView.frame = mapViewFrame
            
//            recognizer.setTranslation(CGPointZero, in: self.view)
//        if ((recognizer.state != UIGestureRecognizerState.Ended) &&
//            (recognizer.state != UIGestureRecognizerState.Failed)) {
//            recognizer.view?.center = recognizer.locationInView(recognizer.view?.superview)
//        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return selectedVisitData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        // create a new cell if needed or reuse an old one
        let cell:VisitTableViewCell = self.visitTableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! VisitTableViewCell
        let dataDict = selectedVisitData[indexPath.row] as! NSDictionary
//        print(dataDict)
        
        cell.timeLabel.text = "\(dataDict["ankunft"]!) - \(dataDict["abfahrt"]!)"
        
        cell.addressLabel.text = "\(dataDict["o_name"]!), \(dataDict["o_strasse"]!), \(dataDict["o_ort"]!)"
        
        let date: Date! = self.formatter.date(from: dataDict["besuchsdatum"]! as! String)

        let calendar = Calendar.current
        
        let dateLabel = calendar.component(.day, from: date)
        let monthLabel = calendar.component(.month, from: date)
        let yearLabel = calendar.component(.year, from: date)
        
        
        let months = self.formatter.shortMonthSymbols
        let monthSymbol = months![monthLabel-1]
        let dayLabel = getDayOfWeek(fromDate: date)
        
        cell.dateLabel.text = "\(dateLabel)"
        cell.monthLabel.text = "\(monthSymbol)"
        cell.yearLabel.text = "\(yearLabel)"
        cell.dayLabel.text = "\(dayLabel!)"
        
        return cell
    }
    
    func getDayOfWeek(fromDate date: Date) -> String?
    {
        let cal = Calendar(identifier: .gregorian)
        let dayOfWeek = cal.component(.weekday, from: date)
        switch dayOfWeek {
        case 1:
            return "Sun"
        case 2:
            return "Mon"
        case 3:
            return "Tue"
        case 4:
            return "Wed"
        case 5:
            return "Thur"
        case 6:
            return "Fri"
        case 7:
            return "Sat"
        default:
            return nil
        }
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
//        print("You tapped cell number \(indexPath.row).")
        let dataDict =  selectedVisitData[indexPath.row] as! NSDictionary
//        print(dataDict)
        
        let visitVC = self.storyboard!.instantiateViewController(withIdentifier: "VisitDetailViewController") as! VisitDetailViewController
        visitVC.dataDict = dataDict
        self.navigationController?.pushViewController(visitVC, animated: true)

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 62
    }

    @IBAction func submitButtonTap(_ sender: UIButton)
    {
        if commentTextView.text.characters.count>0
        {
            mainPopUpView.isHidden = true
            self.addCommentView.isHidden = true

            var datStr: String = ""
            for date in selectedDatesLocal
            {
                let day: String! = self.formatter.string(from: date as Date)
                datStr += "\(day!),"
            }
//            print(datStr)
            
            let truncated = datStr.substring(to: datStr.index(before: datStr.endIndex))
//            print(truncated)
            BackendBase.ShowMBProgress(view: self.view)
            
            let paramsData: [String : AnyObject] = ["kollegenr": appUser.Kollegenr as AnyObject, "day":truncated as AnyObject, "comment":commentTextView.text as AnyObject]
            
            BackendBase.accessApiByPost(apiPath: App_BaseUrl + "/" + Webservice_Cancel, parameters: paramsData, Success: { (result) in
//                print(result)
                for date in self.selectedDatesLocal
                {
                    self.calendarCustom.deselect(date as Date)
                    self.selectedDatesLocal = self.selectedDatesLocal.filter(){$0 as Date != date as Date}
                    self.addType(date: date as Date, remove: true)
                    self.enableDisableButtons()
                    self.configureVisibleCells()
                    self.selectedVisitData = self.getVisitsOnSelectedDates()
                    self.visitTableView.reloadData()
                    
                    let day: String! = self.formatter.string(from: date as Date)
                    
                    let dataDict = self.getStatusOnSelectedDate(date)
//                    print(dataDict)
                    if dataDict["arbeitstatus"]! as! String == "A"
                    {
                        ModelManager.getInstance().updateAvailabilityForAvailable("C", date: day, dateToUpdate: date as Date)
                    }
                }
                self.calendarData.removeAllObjects()
                self.calendarData = ModelManager.getInstance().getAllCalendarData()
                //
                //            self.calendarCustom.reloadData()

                self.commentTextView.text = ""

                BackendBase.HideMBProgress(view: self.view)
                
            }, Failure: { (error) in
                print(error)
                BackendBase.HideMBProgress(view: self.view)
                
            })

        }
        else
        {
            let alert: UIAlertView = UIAlertView()
            alert.message = "Please add comment"
            alert.title = "UGW"
            alert.delegate = self
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    @IBAction func bacButtonTap(_ sender: UIButton)
    {
        mainPopUpView.isHidden = true
        self.addCommentView.isHidden = true
        commentTextView.text = ""
    }
    @IBAction func hideCommentViewTap(_ sender: UIButton)
    {
        mainPopUpView.isHidden = true
        self.addCommentView.isHidden = true
        commentTextView.text = ""
    }
    @IBAction func hideCancelPopUp(_ sender: UIButton)
    {
        mainPopUpView.isHidden = true
        self.PopUpCantCancel.isHidden = true
    }
    
    func getStatusOnSelectedDate(_ date: NSDate) -> NSDictionary
    {
        let day: String! = self.formatter.string(from: date as Date)
        let speciesPredicate = NSPredicate(format: "arbeitsdatum == %@", day)
        let filteredArray = (calendarData as NSMutableArray).filtered(using: speciesPredicate)
        var dataDict: NSDictionary = NSDictionary()
        if filteredArray.count > 0
        {
            dataDict = filteredArray[0] as! NSDictionary
//            print(type(of:dataDict))
            print(dataDict["arbeitstatus"]!)
        }
        return dataDict
    }
}

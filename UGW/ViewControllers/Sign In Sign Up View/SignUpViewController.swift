//
//  SignUpViewController.swift
//  UGW
//
//  Created by Qaiser Abbas on 30/01/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonTap(_ sender: UIButton)
    {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

}

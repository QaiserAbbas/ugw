//
//  SignInViewController.swift
//  UGW
//
//  Created by Qaiser Abbas on 30/01/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController
{

    @IBOutlet var signInView: UIView!
    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var emailImageView: UIImageView!
    @IBOutlet var passwordImageView: UIImageView!
    
    @IBOutlet var signInButton: UIButton!
    @IBOutlet var signUpButton: UIButton!
    @IBOutlet var remeberMeButton: UIButton!
    
    var isRememberMe = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        configureButtonAndViews()
        
        // Do any additional setup after loading the view.
    }
    func configureButtonAndViews()
    {
        signInButton.layer.cornerRadius = 5
        signInButton.clipsToBounds = true
        
        signUpButton.layer.cornerRadius = 5
        signUpButton.clipsToBounds = true
        
        signInView.layer.cornerRadius = 5
        signInView.clipsToBounds = true
        
        emailImageView.layer.cornerRadius = 5
        emailImageView.clipsToBounds = true
        
        passwordImageView.layer.cornerRadius = 5
        passwordImageView.clipsToBounds = true
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonTap(_ sender: UIButton)
    {
        if(emailTextField.text != "" && passwordTextField.text != "" )
        {
            BackendBase.ShowMBProgress(view: self.view)
            let params: [String : AnyObject] = ["username": emailTextField.text as AnyObject , "password": passwordTextField.text as AnyObject ]
            BackendBase.accessApiByPost(apiPath: App_BaseUrl + "/" + Webservice_Login, parameters: params, Success: { (result) in
                print(result)
                let data = result as! NSDictionary
                let status = data["login_status"] as! String
                
                if status == "success"
                {
                    let dict = data["session"] as! NSDictionary
                    Util.saveUser(dict, rememberMe: self.isRememberMe)

                    let paramsData: [String : AnyObject] = ["kollegenr": dict["Kollegenr"] as AnyObject]

                    BackendBase.accessApiByPost(apiPath: App_BaseUrl + "/" + Webservice_Callendar, parameters: paramsData, Success: { (result) in
                        print(result)
                        let dataNew = result as! NSDictionary

                        for i in 0..<dataNew.allKeys.count
                        {
                            let nameOfDict = dataNew.allKeys[i] as! String
                            let dict = dataNew["\(dataNew.allKeys[i])"] as! NSDictionary
                            for i in 0..<dict.allKeys.count
                            {
                                let dataArray = dict["\(dict.allKeys[i])"] as! [Any]
                                var isInserted = false
                                if nameOfDict == "public"
                                {
                                    isInserted = ModelManager.getInstance().save_publicDB(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "promo"
                                {
                                    isInserted = ModelManager.getInstance().save_promo(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "sales"
                                {
                                    isInserted = ModelManager.getInstance().save_sales(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "nestle"
                                {
                                    isInserted = ModelManager.getInstance().save_nestle(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "bsh"
                                {
                                    isInserted = ModelManager.getInstance().save_bsh(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "weber"
                                {
                                    isInserted = ModelManager.getInstance().save_weber(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "tcfhe"
                                {
                                    isInserted = ModelManager.getInstance().save_tcfhe(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "colibri"
                                {
                                    isInserted = ModelManager.getInstance().save_colibri(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                else if nameOfDict == "communication"
                                {
                                    isInserted = ModelManager.getInstance().save_communication(dataArray as NSArray, tabelName: "\(dict.allKeys[i])")
                                }
                                if isInserted
                                {
                                }
                            }
                        }
                        
                        let mainVC = self.storyboard!.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
                        self.navigationController?.pushViewController(mainVC, animated: true)

                        BackendBase.HideMBProgress(view: self.view)
                        
                    }, Failure: { (error) in
                        print(error)
                        BackendBase.HideMBProgress(view: self.view)
                        
                    })
                }
                else
                {
                    BackendBase.HideMBProgress(view: self.view)
                }
                
            }, Failure: { (error) in
                print(error)
                BackendBase.HideMBProgress(view: self.view)
                
            })
        }
    }
    
    @IBAction func rememberMeButtonTap(_ sender: UIButton)
    {
        if sender.isSelected
        {
            sender.isSelected = false
            self.isRememberMe = false
        }
        else
        {
            sender.isSelected = true
            self.isRememberMe = true
        }
        
    }
    
}

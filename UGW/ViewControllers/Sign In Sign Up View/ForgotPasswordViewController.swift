//
//  ForgotPasswordViewController.swift
//  UGW
//
//  Created by Qaiser Abbas on 30/01/2017.
//  Copyright © 2017 Qaiser Abbas. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController
{

    @IBOutlet var forgotView: UIView!
    @IBOutlet var emailImageView: UIImageView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var forgotButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureButtonAndViews()
    {
        forgotButton.layer.cornerRadius = 5
        forgotButton.clipsToBounds = true
        
        
        emailImageView.layer.cornerRadius = 5
        emailImageView.clipsToBounds = true
        
        forgotView.layer.cornerRadius = 5
        forgotView.clipsToBounds = true
    }

    @IBAction func forgotButtonTap(_ sender: UIButton)
    {
    }

    @IBAction func backButtonTap(_ sender: UIButton)
    {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
   
}
